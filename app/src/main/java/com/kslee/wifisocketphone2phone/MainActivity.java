package com.kslee.wifisocketphone2phone;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.Formatter;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class MainActivity extends AppCompatActivity {

	private EditText mIP, mPort, mName;
	private TextView mTextView;
	private Socket socket;
	private DataOutputStream writeSocket;
	private DataInputStream readSocket;
	private Handler mHandler = new Handler();

	private ConnectivityManager cManager;
	private NetworkInfo wifi;
	private ServerSocket serverSocket;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mIP = (EditText) findViewById(R.id.edit_ip);
		mPort = (EditText) findViewById(R.id.edit_port);
		mName = (EditText) findViewById(R.id.edit_name);

		mTextView = (TextView) findViewById(R.id.txt_view);

		cManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

	}

	@SuppressWarnings("deprecation")
	public void OnClick(View v) throws Exception {
		switch (v.getId()) {
			case R.id.btn_connect:
				(new Connect()).start();
				break;
			case R.id.btn_disconnect:
				(new Disconnect()).start();
				break;
			case R.id.btn_setserver:
//				(new SetServer()).start();
				SetServer setServerAsynctask = new SetServer(mPort.getText().toString());
				setServerAsynctask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
				break;
			case R.id.btn_closeserver:
				(new CloseServer()).start();
				break;
			case R.id.btn_viewinfo:
				wifi = cManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
				if (wifi.isConnected()) {
					WifiManager wManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
					WifiInfo info = wManager.getConnectionInfo();
					mTextView.setText("IP Address : " + Formatter.formatIpAddress(info.getIpAddress()));
				} else {
					mTextView.setText("Disconnected");
				}
				break;
			case R.id.btn_msg:
				(new sendMessage()).start();
		}
	}

	class Connect extends Thread {
		public void run() {
			Log.d("Connect", "Run Connect");
			String ip = null;
			int port = 0;

			try {
				ip = mIP.getText().toString();
				port = Integer.parseInt(mPort.getText().toString());
			} catch (Exception e) {
				final String recvInput = "정확히 입력하세요!";
				mHandler.post(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub
						setToast(recvInput);
					}
				});
			}
			try {
				socket = new Socket(ip, port);
				writeSocket = new DataOutputStream(socket.getOutputStream());
				readSocket = new DataInputStream(socket.getInputStream());

				mHandler.post(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub
						setToast("연결에 성공하였습니다.");
					}

				});
				(new recvSocket()).start();
			} catch (Exception e) {
				final String recvInput = "연결에 실패하였습니다.";
				Log.d("Connect", e.getMessage());
				mHandler.post(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub
						setToast(recvInput);
					}

				});

			}

		}
	}

	class Disconnect extends Thread {
		public void run() {
			try {
				if (socket != null) {
					socket.close();
					mHandler.post(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub
							setToast("연결이 종료되었습니다.");
						}
					});

				}

			} catch (Exception e) {
				final String recvInput = "연결에 실패하였습니다.";
				Log.d("Connect", e.getMessage());
				mHandler.post(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub
						setToast(recvInput);
					}

				});

			}

		}
	}

	//thread를 asynctask로 변환
	class SetServer extends AsyncTask<Void,Void,Void> {

		private String mPort;

		public SetServer(String port) {
			this.mPort = port;
		}

		@Override
		protected Void doInBackground(Void... params) {

			try {
				int port = Integer.parseInt(mPort);
				serverSocket = new ServerSocket(port);
				final String result = "서버 포트 " + port + " 가 준비되었습니다.";

				mHandler.post(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub
						setToast(result);
					}
				});

				socket = serverSocket.accept();		//연결될때까지 들어오는 요청을 기다린다.
				writeSocket = new DataOutputStream(socket.getOutputStream());	//write data
				readSocket = new DataInputStream(socket.getInputStream());		//read data

				while (true) {
					byte[] b = new byte[100];	//byte buffer 할당
					int ac = readSocket.read(b, 0, b.length);	//받은 메시지 길이	//socket으로 데이터가 들어올때까지 들어오는 요청을 기다린다.
					String input = new String(b, 0, b.length);	//받은 메시지 String
					final String recvInput = input.trim();		//쓰레기 부분 빼고 trim.

					Log.i("SetServer","b = "+b+" ac = "+ac+" input = "+input+" recvInput = "+recvInput);

					if(ac==-1)	//client 에서 disconnect 했을 때
						break;

					mHandler.post(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub
							setToast(recvInput);
						}

					});
				}
				mHandler.post(new Runnable(){

					@Override
					public void run() {
						// TODO Auto-generated method stub
						setToast("연결이 종료되었습니다.");
					}

				});
				Log.i("SetServer", "serverSocket.close()");
				serverSocket.close();
				socket.close();
			} catch (Exception e) {
				final String recvInput = "서버 준비에 실패하였습니다.";
				Log.d("SetServer", e.getMessage());
				mHandler.post(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub
						setToast(recvInput);
					}

				});

			}

			return null;
		}

		@Override
		protected void onProgressUpdate(Void... values) {
			super.onProgressUpdate(values);
		}
	}

//	class SetServer extends Thread {
//
//		public void run() {
//			try {
//				int port = Integer.parseInt(mPort.getText().toString());
//				serverSocket = new ServerSocket(port);
//				final String result = "서버 포트 " + port + " 가 준비되었습니다.";
//
//				mHandler.post(new Runnable() {
//
//					@Override
//					public void run() {
//						// TODO Auto-generated method stub
//						setToast(result);
//					}
//				});
//
//				socket = serverSocket.accept();		//연결될때까지 들어오는 요청을 기다린다.
//				writeSocket = new DataOutputStream(socket.getOutputStream());	//write data
//				readSocket = new DataInputStream(socket.getInputStream());		//read data
//
//				while (true) {
//					byte[] b = new byte[100];	//byte buffer 할당
//					int ac = readSocket.read(b, 0, b.length);	//받은 메시지 길이	//socket으로 데이터가 들어올때까지 들어오는 요청을 기다린다.
//					String input = new String(b, 0, b.length);	//받은 메시지 String
//					final String recvInput = input.trim();		//쓰레기 부분 빼고 trim.
//
//					Log.i("SetServer","b = "+b+" ac = "+ac+" input = "+input+" recvInput = "+recvInput);
//
//					if(ac==-1)	//client 에서 disconnect 했을 때
//						break;
//
//					mHandler.post(new Runnable() {
//
//						@Override
//						public void run() {
//							// TODO Auto-generated method stub
//							setToast(recvInput);
//						}
//
//					});
//				}
//				mHandler.post(new Runnable(){
//
//					@Override
//					public void run() {
//						// TODO Auto-generated method stub
//						setToast("연결이 종료되었습니다.");
//					}
//
//				});
//				Log.i("SetServer", "serverSocket.close()");
//				serverSocket.close();
//				socket.close();
//			} catch (Exception e) {
//				final String recvInput = "서버 준비에 실패하였습니다.";
//				Log.d("SetServer", e.getMessage());
//				mHandler.post(new Runnable() {
//
//					@Override
//					public void run() {
//						// TODO Auto-generated method stub
//						setToast(recvInput);
//					}
//
//				});
//
//			}
//
//		}
//	}

	class recvSocket extends Thread {

		public void run() {
			try {
				readSocket = new DataInputStream(socket.getInputStream());

				while (true) {
					byte[] b = new byte[100];
					int ac = readSocket.read(b, 0, b.length);
					String input = new String(b, 0, b.length);
					final String recvInput = input.trim();

					if(ac==-1)
						break;

					mHandler.post(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub
							setToast(recvInput);
						}

					});
				}
				mHandler.post(new Runnable(){

					@Override
					public void run() {
						// TODO Auto-generated method stub
						setToast("연결이 종료되었습니다.");
					}

				});
			} catch (Exception e) {
				final String recvInput = "연결에 문제가 발생하여 종료되었습니다..";
				Log.d("recvSocket", e.getMessage());
				mHandler.post(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub
						setToast(recvInput);
					}

				});

			}

		}
	}

	class CloseServer extends Thread {
		public void run() {
			try {
				if (serverSocket != null) {
					serverSocket.close();
					socket.close();

					mHandler.post(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub
							setToast("서버가 종료되었습니다..");
						}
					});
				}
			} catch (Exception e) {
				final String recvInput = "서버 준비에 실패하였습니다.";
				Log.d("CloseServer", e.getMessage());
				mHandler.post(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub
						setToast(recvInput);
					}

				});

			}

		}
	}

	class sendMessage extends Thread {
		public void run() {
			try {
				byte[] b = new byte[100];
				b = "Hello, KYOSEON LEE!".getBytes();
				writeSocket.write(b);

			} catch (Exception e) {
				final String recvInput = "메시지 전송에 실패하였습니다.";
				Log.d("sendMessage", e.getMessage());
				mHandler.post(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub
						setToast(recvInput);
					}

				});

			}

		}
	}

	void setToast(String msg) {
		Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.menu_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		//noinspection SimplifiableIfStatement
		if (id == R.id.action_settings) {
			return true;
		}

		return super.onOptionsItemSelected(item);
	}
}
